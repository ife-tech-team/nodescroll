/**
 * Created by oluwadara on 8/26/16.
 */

var elasticsearch = require('elasticsearch');
var fs = require('fs');
var exec = require('child_process').exec;
var numLines;

var elasticClient = new elasticsearch.Client({
    host: 'http://162.13.221.160:10200',
    log: 'trace'
});

elasticClient.search({
        index : 'sub_info',
        type : 'subscriber',
        scroll: '5s',
        search_type: 'scan',
        body: {
            query : {
                match: {
                    service_id : "10024"
                }
            }
        }
    },
    function getMoreUntilDone(error, response) {
        response.hits.hits.forEach(function (hit) {
            fs.appendFile("records.txt", hit._source.user_id + " " + hit._source.status + " " +  hit._source.transaction_time + "\n", function (err) {
                if (err)
                    return console.log(err);
                console.log("Written to file");
            });


        });

        exec('wc records.txt', function (error, results) {
            if(error) console.log(error);
            numLines = parseInt(results, 10);
            console.log(numLines);
        });

        if (response.hits.total !== numLines ) {
            elasticClient.scroll({
                scrollId: response._scroll_id,
                scroll: '5s'
            }, getMoreUntilDone);
        }

        else {
            console.log('Done');
        }

});

